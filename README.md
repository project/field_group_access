# Field Group Access Module


## Introduction

The Field Group Access module adds permissions for editing and viewing fields inside field groups.

At present this is only done for fields on forms and for the default form display.


## Requirements

No special requirements.


## Installation

Install as you would normally install a contributed Drupal module. See: https://www.drupal.org/documentation/install/modules-themes/modules-8

Once installed, permissions will be added in for all roles for all field groups. Note that by default anon and authenticated are given permissions to edit and view all field groups.


## Maintainers

Current Maintainers:
* Michael Welford - https://www.drupal.org/u/mikejw

This project has been sponsored by:
* The University of Adelaide - https://www.drupal.org/university-of-adelaide
  Visit: http://www.adelaide.edu.au
